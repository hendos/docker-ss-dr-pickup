#!/bin/sh
# Copyright (C) 2016  Henderson Group

set -e
set -x


data_dir="${1:?specify path to FTP data (home) directory}"

if [[ ! -d "${data_dir}" ]]; then
  echo "FTP data (home) directory does not exist: ${data_dir}" >&2
  exit 1
fi

cd "${data_dir}"

if [[ ! -d __dr_pickup ]]; then
  mkdir __dr_pickup
fi

# -9 (max compression), -r (recurse dirs), -m (move into zip),
# -j (don't record paths), -o (make zip as old as latest entry),
# -x (exclude backups), -i (only include pad files)
zip -9 -r -m -j -o \
  __dr_pickup/ss-dr--$(date +%Y%m%d-%H%M%S).zip * \
  -x '*__dr_pickup*' -i '*.pad'

# give user 10min to pickup and shut down
exec \
  timeout -t 600 \
  darkhttpd "$PWD/__dr_pickup" --chroot --uid darkhttpd --gid nogroup
